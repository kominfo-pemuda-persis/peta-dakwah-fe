import { extend } from 'vee-validate'
import { required, numeric } from 'vee-validate/dist/rules'

extend('required', {
  ...required,
  params: ['isHidden'],
  message: 'Form ini dibutuhkan',
})

extend('numeric', {
  ...numeric,
  message: 'Form ini hanya mendukung format angka',
})

extend('phone', {
  validate: (value) => value.match(/^(\+62|62|0)8[1-9][0-9]{6,9}$/),
  message: 'Masukan nomor telepon dengan valid',
})

extend('after', {
  params: ['targetDate'],
  validate: (value, { targetDate }) => {
    if (Date.parse(value) > Date.parse(targetDate)) {
      return true
    }
    return false
  },
  message:
    'tanggal periode end tidak boleh lebih kecil atau sama dengan tanggal periode start',
})

extend('before', {
  params: ['targetDate'],
  validate: (value, { targetDate }) => {
    if (Date.parse(value) < Date.parse(targetDate)) {
      return true
    }
    return false
  },
  message:
    'tanggal periode start tidak boleh lebih besar atau sama dengan tanggal periode end',
})

extend('email', {
  validate: (value) =>
    value.match(
      // eslint-disable-next-line no-useless-escape
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    ),
  message: 'Masukan email aktif dengan valid',
})
