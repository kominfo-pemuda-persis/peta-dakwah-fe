export default (_app, inject) => {
  inject('util', {
    debounce(fn, delay) {
      let timeoutID = null
      return function () {
        clearTimeout(timeoutID)
        const args = arguments
        const that = this
        timeoutID = setTimeout(function () {
          fn.apply(that, args)
        }, delay)
      }
    },
    randomAlphaNum() {
      return Math.random().toString(36).slice(2)
    },
    setIndexTable(index, number, size) {
      return index + 1 + number * size
    },
    setLabelTotalInfo(pagination) {
      return `Showing ${pagination.number * pagination.size + 1} - ${
        (pagination.number + 1) * pagination.size
      } of ${pagination.totalElements} entries`
    },
    showModal({ type = 'success', callback = null, text, context = null }) {
      document.querySelector(`#modal-${type}`).checked = true
      context.$emit(`modal-${type}-opened`, {
        callback,
        text,
      })
    },
    getDateNativeHTML(date) {
      return date.toISOString().split('T')[0]
    },
    getDateBackendFormat(rawDate) {
      const date = rawDate.getDate()
      const month = rawDate.getMonth() + 1
      const year = rawDate.getFullYear()
      let format = ''
      if (date < 10) {
        format = format.concat(`0${date}-`)
      } else {
        format = format.concat(`${date}-`)
      }
      if (month < 10) {
        format = format.concat(`0${month}-`)
      } else {
        format = format.concat(`${month}-`)
      }
      format = format.concat(year)
      return format
    },
    setupCommonQueryParam({ route, router, params }) {
      router.replace({
        query: {
          ...route.query,
          ...params,
        },
      })
    },
  })
}
