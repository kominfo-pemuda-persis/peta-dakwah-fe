module.exports = {
  plugins: [
    {
      rules: {
        kominfo: ({ raw }) => {
          const format =
            '<spasi>#<nomer-issue><spasi>|<spasi><nama><spasi>|<spasi><commit-message>'
          const title = 'FORMAT COMMIT KOMINFO PEMUDAH PERSIS'
          const description = 'Format message yang benar'
          const rightFormat = '<spasi>#400 | Irwan | contoh commit'
          if (!raw.match(/\s#\d+\s\|\s\w+\s\|\s.+/))
            return [
              false,
              `\n${title}\n\n${description} ${format}\nContoh: ${rightFormat}`,
            ]
          return [true, '']
        },
      },
    },
  ],
  extends: ['@commitlint/config-conventional'],
  rules: { kominfo: [2, 'always'] },
}
