import { KEADAAN_JAMAAH } from '~/utilities/endpoints'
export const actions = {
  async getList(_vuex, { page = 0, size = 10, keyword = '' }) {
    return await this.$axios.get(KEADAAN_JAMAAH, {
      params: { page, size, keyword },
    })
  },
  async save(_vuex, { payload }) {
    return await this.$axios.post(KEADAAN_JAMAAH, payload)
  },
  async update(_vuex, { id, payload }) {
    return await this.$axios.put(`${KEADAAN_JAMAAH}/${id}`, payload)
  },
  async delete(_vuex, id) {
    return await this.$axios.delete(`${KEADAAN_JAMAAH}/${id}`)
  },
  async get(_vuex, id) {
    return await this.$axios.get(`${KEADAAN_JAMAAH}/${id}`)
  },
}

export const state = () => ({
  form: null,
})

export const mutations = {
  setForm(state, payload) {
    state.form = payload
  },
}

export const getters = {
  form(state) {
    return state.form
  },
}
