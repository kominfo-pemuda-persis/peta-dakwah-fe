export const actions = {
  async getListKoordinatKota(_vuex, { page = 0, size = 10, keyword = '' }) {
    return await this.$axios.get('api/cities-coordinate', {
      params: { page, size, keyword },
    })
  },
  async addKoordinatKota(
    _vuex,
    {
      kode,
      nama,
      ibukota,
      latitude,
      longitude,
      elevation,
      timeZone,
      luas,
      penduduk,
      status,
      path,
    }
  ) {
    return await this.$axios.post('api/cities-coordinate', {
      kode,
      nama,
      ibukota,
      latitude,
      longitude,
      elevation,
      timeZone,
      luas,
      penduduk,
      status,
      path,
    })
  },
  async deleteKoordinatKota(_vuex, { id }) {
    return await this.$axios.delete(`api/cities-coordinate/${id}`)
  },
}

export const state = () => ({
  form: null,
})

export const mutations = {
  setForm(state, payload) {
    state.form = payload
  },
}

export const getters = {
  form(state) {
    return state.form
  },
}
