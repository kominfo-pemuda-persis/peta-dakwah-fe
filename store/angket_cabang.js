import { ANGKET_CABANG } from '~/utilities/endpoints'
export const actions = {
  async getList(_vuex, { page = 0, size = 10, keyword = '' }) {
    return await this.$axios.get(ANGKET_CABANG, {
      params: { page, size, keyword },
    })
  },
  async save(_vuex, { payload }) {
    return await this.$axios.post(ANGKET_CABANG, payload)
  },
  async update(_vuex, { id, payload }) {
    return await this.$axios.put(`${ANGKET_CABANG}/${id}`, payload)
  },
  async delete(_vuex, id) {
    return await this.$axios.delete(`${ANGKET_CABANG}/${id}`)
  },
  async get(_vuex, id) {
    return await this.$axios.get(`${ANGKET_CABANG}/${id}`)
  },
}

export const state = () => ({
  form: null,
})

export const mutations = {
  setForm(state, payload) {
    state.form = payload
  },
}

export const getters = {
  form(state) {
    return state.form
  },
}
