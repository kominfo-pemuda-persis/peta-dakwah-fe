import { MUBALLIGH } from '~/utilities/endpoints'
export const actions = {
  async getListPofilMuballigh(_vuex, { page = 0, size = 10, keyword = '' }) {
    return await this.$axios.get(MUBALLIGH, {
      params: { page, size, keyword },
    })
  },
  async save(_vuex, { payload }) {
    return await this.$axios.post(MUBALLIGH + '/', {
      ...payload,
      createdAt: new Date().toISOString(),
    })
  },
  async update(_vuex, { id, payload }) {
    return await this.$axios.put(
      `${MUBALLIGH}/${id}`,
      {
        ...payload,
        updatedAt: new Date().toISOString(),
      },
      {
        params: { id },
      }
    )
  },
  async delete(_vuex, id) {
    return await this.$axios.delete(`${MUBALLIGH}/${id}`)
  },
  async get(_vuex, id) {
    return await this.$axios.get(`${MUBALLIGH}/${id}`)
  },
}

export const state = () => ({
  form: null,
})

export const mutations = {
  setForm(state, payload) {
    state.form = payload
  },
}

export const getters = {
  form(state) {
    return state.form
  },
}
