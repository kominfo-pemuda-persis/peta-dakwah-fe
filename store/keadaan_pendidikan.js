import { KEADAAN_PENDIDIKAN } from '~/utilities/endpoints'

export const actions = {
  async getList(_vuex, { page = 0, size = 10, keyword = '' }) {
    return await this.$axios.get('api/pendidikan', {
      params: { page, size, keyword },
    })
  },
  async save(_vuex, { payload }) {
    return await this.$axios.post(KEADAAN_PENDIDIKAN, payload)
  },
  async update(_vuex, { id, payload }) {
    return await this.$axios.put(`${KEADAAN_PENDIDIKAN}/${id}`, payload)
  },
  async delete(_vuex, id) {
    return await this.$axios.delete(`${KEADAAN_PENDIDIKAN}/${id}`)
  },
  async get(_vuex, id) {
    return await this.$axios.get(`${KEADAAN_PENDIDIKAN}/${id}`)
  },
}

export const state = () => ({
  form: null,
})

export const mutations = {
  setForm(state, payload) {
    state.form = payload
  },
}

export const getters = {
  form(state) {
    return state.form
  },
}
