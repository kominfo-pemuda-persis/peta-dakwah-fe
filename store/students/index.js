import { STUDENTS } from '~/utilities/endpoints'

export const actions = {
  async save(ctx, payload) {
    const url = `${STUDENTS}`
    try {
      const res = await this.$axios.$post(url, payload)
      return res
    } catch (error) {
      if (error.response) {
        if (error.response.status === 401) {
          throw new Error('Bad Credentials')
        } else if (error.response.status === 502) {
          throw new Error('Network Error')
        }
      }
      throw error
    }
  },
}

export const state = () => ({
  form: null,
})

export const mutations = {
  setForm(state, payload) {
    state.form = payload
  },
}

export const getters = {
  form(state) {
    return state.form
  },
}
