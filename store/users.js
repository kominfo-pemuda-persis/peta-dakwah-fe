import { USERS } from '~/utilities/endpoints'

export const actions = {
  async getListUsers(_vuex, { page = 0, size = 1, keyword = '', type = '' }) {
    return await this.$axios.get(USERS, {
      params: { page, size, [type]: keyword },
    })
  },
  async getUser(_vuex, id) {
    return await this.$axios.get(`${USERS}/${id}`)
  },
}
