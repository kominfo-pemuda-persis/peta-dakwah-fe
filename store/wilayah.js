export const actions = {
  async getListProvinsi(_vuex, { keyword, page = 0, size = 10 }) {
    return await this.$axios.get('api/provinces', {
      params: { keyword, page, size },
    })
  },

  async getListKota(
    _vuex,
    { keyword, page = 0, size = 10, 'kode-provinsi': kodeProvinsi }
  ) {
    return await this.$axios.get('api/districts', {
      params: { keyword, page, size, 'kode-provinsi': kodeProvinsi },
    })
  },

  async getListKecamatan(
    _vuex,
    { keyword, page = 0, size = 10, 'kode-kota': kodeKota }
  ) {
    return await this.$axios.get('api/sub-districts', {
      params: { keyword, page, size, 'kode-kota': kodeKota },
    })
  },

  async getListKelurahan(
    _vuex,
    { keyword, page = 0, size = 10, 'kode-kecamatan': kodeKecamatan }
  ) {
    return await this.$axios.get('api/villages', {
      params: { keyword, page, size, 'kode-kecamatan': kodeKecamatan },
    })
  },

  async getListPW(_vuex, { keyword, page = 0, size = 10 }) {
    return await this.$axios.get('api/pw', {
      params: { keyword, page, size },
    })
  },

  async getListPD(_vuex, { keyword, page = 0, size = 10, 'kode-pw': kodePW }) {
    return await this.$axios.get('api/pd', {
      params: { keyword, page, size, 'kode-pw': kodePW },
    })
  },

  async getListPC(_vuex, { keyword, page = 0, size = 10, 'kode-pd': kodePD }) {
    return await this.$axios.get('api/pc', {
      params: { keyword, page, size, 'kode-pd': kodePD },
    })
  },

  async getCoordinate(_vuex, { id }) {
    return await this.$axios.get(`/api/cities-coordinate/${id}`)
  },
}

export const state = () => ({
  form: null,
})

export const mutations = {
  setForm(state, payload) {
    state.form = payload
  },
}

export const getters = {
  form(state) {
    return state.form
  },
}
