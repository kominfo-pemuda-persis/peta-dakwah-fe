import { LOGIN } from './utilities/endpoints'

export default {
  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',
  ssr: false,

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Wilayah Dakwah',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['@/assets/css/main.css'],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: ['~/plugins/util.plugins.js', '@/plugins/vee-validate/index.js'],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/stylelint
    '@nuxtjs/stylelint-module',
    // manual tailwind integration
    '@nuxt/postcss8',
    '@nuxtjs/svg',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://auth.nuxtjs.org/
    '@nuxtjs/auth-next',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    // https://github.com/schlunsen/nuxt-leaflet
    'nuxt-leaflet',
    // 'bootstrap-vue/nuxt',
    '@nuxtjs/sentry',
  ],

  router: {
    middleware: ['auth'],
  },

  auth: {
    strategies: {
      custom: {
        scheme: '~/schemes/custom',
        token: {
          property: 'access_token',
          maxAge: 60 * 60 * 24 * 30,
          global: true,
          type: 'Bearer',
        },
        refreshToken: {
          property: 'refresh_token',
          data: 'refresh_token',
          maxAge: 60 * 60 * 24 * 30,
        },
        user: {
          autoFetch: true,
        },
        endpoints: {
          login: { url: LOGIN, method: 'post' },
          refresh: { url: '/api/auth/refresh', method: 'post' },
          user: false,
          logout: { url: '/api/auth/logout', method: 'post' },
        },
        redirect: {
          login: '/login',
          logout: '/',
          callback: '/login',
          home: '/',
        },
        // autoLogout: false
      },
    },
    localStorage: false,
    cookie: {
      options: {
        expires: 1,
      },
    },
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {},

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'en',
    },
  },

  sentry: {
    dsn: process.env.SENTRY_DSN, // Enter your project's DSN.
    config: {},
    tracing: true,
  },

  svg: {
    vueSvgLoader: {
      // vue-svg-loader options
    },
    svgSpriteLoader: {
      // svg-sprite-loader options
    },
    fileLoader: {
      // file-loader options
    },
  },

  publicRuntimeConfig: {
    // Axios module configuration: https://go.nuxtjs.dev/config-axios
    axios: {
      baseUrl:
        process.env.API_URL || 'https://petadakwah-apidev.persis.or.id/api/',
    },
    envMode: process.env.ENV_MODE || '',
  },

  server: {
    port: process.env.PORT || 3000,
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    babel: {
      plugins: [
        ['@babel/plugin-proposal-private-property-in-object', { loose: true }],
      ],
    },
    transpile: ['vee-validate/dist/rules'],
    postcss: {
      plugins: {
        tailwindcss: {},
        autoprefixer: {},
      },
    },
  },
}
