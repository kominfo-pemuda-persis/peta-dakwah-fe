// side menu list
export const MENU = [
  {
    title: 'Dashboard',
    slug: 'dashboard',
    icon: 'Home',
    to: '/',
  },
  {
    title: 'Peta Wilayah',
    slug: 'peta-wilayah',
    icon: 'Globe',
    to: '/maps',
  },
  {
    title: 'Angket Cabang',
    slug: 'angket-cabang',
    icon: 'Book',
    to: '/angket-cabang',
  },
  {
    title: 'Keadaan Pendidikan',
    slug: 'keadaan-pendidikan',
    icon: 'Profile',
    to: '/keadaan-pendidikan',
  },
  {
    title: "Keadaan Jama'ah",
    slug: 'keadaan-jamaah',
    icon: 'Profile',
    to: '/keadaan-jamaah',
  },
  {
    title: 'Profil Muballigh',
    slug: 'profil-muballigh',
    icon: 'Profile',
    to: '/profil-muballigh',
  },
  {
    title: 'Koordinat Kota',
    slug: 'city-coordinate',
    icon: 'Globe',
    to: '/koor-kota',
  },
  {
    title: 'List Of Users',
    slug: 'users',
    icon: 'Profile',
    to: '/users',
  },
]

export const geoJsonExample = {
  type: 'FeatureCollection',
  features: [
    {
      type: 'Feature',
      properties: {},
      geometry: {
        coordinates: [
          [
            [107.58102518901131, -6.830846103411588],
            [107.56780189508117, -6.61744576872006],
            [107.20746713551267, -6.814434034622536],
            [107.11820990148937, -7.204885870357174],
            [107.66367077606719, -7.2245637671802],
            [107.98433565385, -7.063836077831979],
            [107.84218524411074, -6.847257609074759],
            [107.76284548053616, -6.666700293945766],
            [107.58102518901131, -6.830846103411588],
          ],
        ],
        type: 'Polygon',
      },
    },
  ],
}
