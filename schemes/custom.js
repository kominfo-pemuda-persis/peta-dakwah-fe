import { RefreshScheme } from '~auth/runtime'

export default class CustomScheme extends RefreshScheme {
  // Override `login` method of `cookie` scheme
  async login(endpoint, { reset = true } = {}) {
    if (!this.options.endpoints.login) {
      return
    }
    // Ditch any leftover local tokens before attempting to log in
    if (reset) {
      this.$auth.reset({ resetInterceptor: false })
    }
    // Add client id to payload if defined
    if (this.options.clientId) {
      endpoint.data.client_id = this.options.clientId
    }
    // Add grant type to payload if defined
    if (this.options.grantType) {
      endpoint.data.grant_type = this.options.grantType
    }
    // Add scope to payload if defined
    if (this.options.scope) {
      endpoint.data.scope = this.options.scope
    }

    // Make login request
    const response = await this.$auth.request(
      endpoint,
      this.options.endpoints.login
    )
    // Update tokens
    this.updateTokens(response)
    // eslint-disable-next-line no-unused-vars, camelcase
    const { access_token, refresh_token, type, ...user } = response.data
    localStorage.setItem('user', JSON.stringify(user))
    // Initialize request interceptor if not initialized
    if (!this.requestHandler.interceptor) {
      this.initializeRequestInterceptor()
    }
    // Fetch user if `autoFetch` is enabled
    if (this.options.user.autoFetch) {
      await this.fetchUser()
    }
    return response
  }

  // Override `fetchUser` method of `cookie` scheme
  fetchUser() {
    // Token is required but not available
    const check = this.check()
    if (!check.valid) {
      return
    }

    try {
      const userDetail = JSON.parse(localStorage.getItem('user'))
      if (!userDetail) {
        throw new Error(`Storage does not contain User Data`)
      }
      this.$auth.setUser(userDetail)
      return userDetail
    } catch (error) {
      this.$auth.callOnError(error, { method: 'fetchUser' })
    }
  }

  logout() {
    return super.reset()
  }

  reset({ resetInterceptor = true } = {}) {
    this.$auth.setUser(false)
    localStorage.removeItem('user')
    // this.$auth.$storage.removeLocalStorage('user')
    this.token.reset()
    this.refreshToken.reset()

    if (resetInterceptor) {
      this.requestHandler.reset()
    }
  }
}
